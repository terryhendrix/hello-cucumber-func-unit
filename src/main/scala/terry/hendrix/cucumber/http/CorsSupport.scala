package terry.hendrix.cucumber.http

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.{Directive0, Route}

trait CorsSupport {
  this:RestApi with ExtendableRoutes ⇒

  private lazy val allowedOrigin = {
    val sAllowedOrigin = config.getString("rest.cors.allowed-origin")
    if(sAllowedOrigin.trim == "*")
      `Access-Control-Allow-Origin`.*
    else
      `Access-Control-Allow-Origin`( HttpOrigin(sAllowedOrigin))
  }

  private lazy val allowHeaders = config.getString("rest.cors.allowed-headers").trim.replaceAll("\\s+", "").split(",")

  //this directive adds access control headers to normal responses
  private def addAccessControlHeaders: Directive0 = {
    mapResponseHeaders { headers =>
      allowedOrigin +:
        `Access-Control-Allow-Credentials`(allowHeaders.contains("Authorization")) +:
        `Access-Control-Allow-Headers`(allowHeaders:_*) +:
        headers
    }
  }

  //this handles preflight OPTIONS requests. TODO: see if can be done with rejection handler,
  //otherwise has to be under addAccessControlHeaders
  private def preflightRequestHandler: Route = options {
    complete {
      HttpResponse(200).withHeaders(
        `Access-Control-Allow-Methods`(OPTIONS, POST, PUT, GET, DELETE)
      )
    }
  }

  private def corsSupport(r: Route) = addAccessControlHeaders {
    preflightRequestHandler ~ r
  }

  wrapRoute(corsSupport)
}