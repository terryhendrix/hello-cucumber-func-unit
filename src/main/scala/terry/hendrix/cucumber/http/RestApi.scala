package terry.hendrix.cucumber.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.RouteResult.route2HandlerFlow
import akka.http.scaladsl.server._
import com.typesafe.config.Config
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

object RestApi {
  object Settings extends ExtendedTypeSafeConfig {
    def emptyStringToNone(s:String) =  if(s.trim.isEmpty) None else Some(s)

    def apply(config:Config):Settings = {
      new Settings(
        config.getString(s"rest.interface"),
        config.getInt(s"rest.port"),
        config.getFiniteDuration(s"rest.timeout"),
        Try(emptyStringToNone(config.getString(s"rest.username"))).getOrElse(None),
        Try(emptyStringToNone(config.getString(s"rest.password"))).getOrElse(None)
      )
    }
  }

  case class Settings(interface:String, port:Int, timeout:FiniteDuration, username:Option[String], password:Option[String])
}

trait RestApiLifeCycle {
  def preStart(): Unit = { }
  def postStop(): Unit = { }
}

trait RestApi extends Directives with RestApiLifeCycle
{
  implicit def mat:akka.stream.Materializer
  def config:Config
  def route:Route
  def system:ActorSystem

  def defaultExceptionHandler =
    ExceptionHandler {
      case e: Throwable =>
        extractUri { uri =>
          extractHost { host ⇒
            system.log.warning("Error while calling {} from {}: {} - {}", uri, host, e.getClass.getName, e.getMessage)
            complete(StatusCodes.InternalServerError)
          }
        }
    }

  lazy val settings:RestApi.Settings =  RestApi.Settings(config)

  def initializeRestApi(implicit system:ActorSystem): Future[ServerBinding] = {
    preStart()
    system.log.info(s"Loading rest api on ${settings.interface}:${settings.port}")
    Http().bindAndHandle(interface = settings.interface, port = settings.port, handler = route2HandlerFlow(handleExceptions(defaultExceptionHandler){
      route
    }))
  }

}
