package terry.hendrix.cucumber.http

import akka.http.scaladsl.server._
import terry.hendrix.cucumber.http.ExtendableRoutes.{RouteWrapper, InternalExtendedRoute}

object ExtendableRoutes{
  type RouteWrapper = Route ⇒ Route
  private[http] type InternalExtendedRoute = () ⇒ Route
}

trait ExtendableRoutes
{ this:RouteConcatenation ⇒

  type Exhausting = Boolean
  private var registeredRoutes:Seq[(InternalExtendedRoute, Exhausting)] =  Seq.empty
  private var wrappedDirectives:Seq[RouteWrapper] =  Seq.empty
  var hasExhausting = false

  /**
   *
   * @param route       a function to create a `Route`
   * @param exhausting
   */
  def extendRoute(route:  ⇒ Route, exhausting:Boolean = false) = {
    if(registeredRoutes.exists(_._2) && exhausting)
      throw new Exception("Cannot have 2 exhausting routes")

    registeredRoutes = registeredRoutes :+ (() ⇒ route, exhausting)
  }

  def wrapRoute(wrapper:RouteWrapper) = {
    wrappedDirectives = wrappedDirectives :+ wrapper
  }

  lazy final val route: Route = {
    val nonExhausting = registeredRoutes.filter(!_._2).map(_._1).foldLeft(extendedRoute){ (a,b) ⇒
      a ~ b()
    }

    val chain = registeredRoutes.find(_._2).map(_._1).map { exhaustingRoute ⇒
      nonExhausting ~ exhaustingRoute()
    }.getOrElse(nonExhausting)

    wrappedDirectives.foldLeft(chain){ case (route,wrapper) ⇒
      wrapper(route)
    }
  }

  def extendedRoute: Route
}
