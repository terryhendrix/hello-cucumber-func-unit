package terry.hendrix.cucumber.http

import akka.actor.ActorSystem
import com.typesafe.config.Config

import scala.io.Source

object ViewRoute {
  private[http] object Settings extends ExtendedTypeSafeConfig {
    def apply(config:Config):Settings = {
      new Settings(
        config.getString("rest.view-route.path-to-resources"),
        config.getString("rest.view-route.web-dir"),
        config.getBoolean(s"rest.view-route.developer-mode")
      )
    }
  }

  private[http] case class Settings(pathToResources:String, webDir:String, developerMode:Boolean)
}

trait ViewRoute
{ this:RestApi with ExtendableRoutes ⇒
  def system:ActorSystem

  protected lazy val viewRouteSettings = ViewRoute.Settings(config)

  extendRoute {
    system.log.info(s"Running view route with $viewRouteSettings")
    pathSingleSlash {
      getResource(s"${viewRouteSettings.webDir}/index.html")
    } ~ getResourceFromDir(viewRouteSettings.webDir)
  }

  implicit class AddTrailingSlash(s:String) {
    def addTrailingSlash = if(s.endsWith("/")) s else s + "/"
  }

  def getResourceFromDir(path: String) = {
    if (viewRouteSettings.developerMode) {
      getFromDirectory(viewRouteSettings.pathToResources.addTrailingSlash + path)
    }
    else {
      getFromResourceDirectory(path)
    }
  }

  def getResource(path:String) = {
    if(viewRouteSettings.developerMode) {
      getFromDirectory(viewRouteSettings.pathToResources.addTrailingSlash + path)
    }
    else {
      getFromResource(path)
    }
  }

  def getResourceAsString(path:String) = {
    if(viewRouteSettings.developerMode)
      Source.fromFile(viewRouteSettings.pathToResources.addTrailingSlash + path).mkString
    else
      Source.fromURL(getClass.getResource("/"+path)).mkString
  }
}

