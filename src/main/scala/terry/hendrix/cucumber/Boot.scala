package terry.hendrix.cucumber
import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.Config
import terry.hendrix.cucumber.http.{ExtendableRestApi, ViewRoute}

object Boot extends App with ExtendableRestApi with ViewRoute
{
  override implicit def system: ActorSystem = ActorSystem("Cucumber")

  override val config: Config = system.settings.config

  override implicit val mat: Materializer = ActorMaterializer()

  override def extendedRoute: Route = {
    getFromDirectory("web")
  }

  initializeRestApi
}
