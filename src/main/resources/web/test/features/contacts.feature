Feature: Sample Application
  As an end user
  I want to store contact information about my contacts
  So that i can contact them without having to memorize their information

  Scenario: User sees loads application for the first time
    Given The page has loaded
    When The front page is visible
    Then I should see my contact information