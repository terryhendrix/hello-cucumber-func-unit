
this.World = function() {};

// Define your World!
//
this.World.prototype.variable = 0;

this.World.prototype.setTo = function(number) {
    this.variable = parseInt(number);
};

this.World.prototype.incrementBy = function(number) {
    this.variable += parseInt(number);
};

this.World.prototype.failTest = function(message) {
    console.error(message);
    throw new Error(message);
}

/////// Your step definitions /////

// use this.Given(), this.When() and this.Then() to declare step definitions

// Alternatively you can use this.defineStep and even use it
// like this:

var Given = When = Then = this.defineStep;

Given(/^The page has loaded$/, function() {
    console.log('[CUCUMBER] Given: The page has loaded');
});

When(/^The front page is visible$/, function() {
    console.log('[CUCUMBER] When: The front page is visible');
    //if(!$("#contacts-app").length) {
    //    this.failTest("There is no body visible");
    //}
});

Then(/^I should see my contact information$/, function() {
    console.log('[CUCUMBER] Then: I should see my contact information');

    if(!$("#contacts-panel").length) {
        this.failTest("Contacts panel is not visible while it should be");
    }
});