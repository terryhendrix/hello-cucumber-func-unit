/**
 * A sample application
 */
var app = angular.module("contacts-app", []);

app.controller("ContactsController", function($scope) {
    $scope.list = [];

    $scope.addRow = function(name, phone, mail) {
        console.log("Adding row");

        $scope.list.push({
            name:name,
            phone:phone,
            mail:mail
        });

        $("#addRowModal").modal("hide");

        //clear out the text fields
        delete $scope.name;
        delete $scope.phone;
        delete $scope.mail;
    };

    $scope.deleteRow = function(row) {
        console.log("Deleting row");

        $scope.list = $scope.list.filter(function(entry) {
            return entry != row;
        })
    };

    $scope.clear = function() {
        console.log("Clearing list");
        $scope.list = [];
    };

    console.log("Loaded app controller");
});


app.directive("contactList", function(){
    return {
        templateUrl: "contacts.html",
        controller: "ContactsController"
    }
});