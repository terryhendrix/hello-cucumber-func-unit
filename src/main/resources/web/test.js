
var test = angular.module("test-app", []);
test.controller("TestController", function($scope, $http) {
    $scope.features = [
        { name: "math" },
        { name: "contacts" }
    ];


    function CucumberHTMLListener($root) {
        var formatter = new CucumberHTML.DOMFormatter($root);

        formatter.uri('report.feature');

        var currentStep;

        var self = {
            hear: function hear(event, callback) {
                var eventName = event.getName();
                switch (eventName) {
                    case 'BeforeFeature':
                        var feature = event.getPayloadItem('feature');
                        formatter.feature({
                            keyword     : feature.getKeyword(),
                            name        : feature.getName(),
                            line        : feature.getLine(),
                            description : feature.getDescription()
                        });
                        break;

                    case 'BeforeScenario':
                        var scenario = event.getPayloadItem('scenario');
                        formatter.scenario({
                            keyword     : scenario.getKeyword(),
                            name        : scenario.getName(),
                            line        : scenario.getLine(),
                            description : scenario.getDescription()
                        });
                        break;

                    case 'BeforeStep':
                        var step = event.getPayloadItem('step');
                        self.handleAnyStep(step);
                        break;

                    case 'StepResult':
                        var result;
                        var stepResult = event.getPayloadItem('stepResult');
                        if (stepResult.getStatus() === Cucumber.Status.FAILED) {
                            var error = stepResult.getFailureException();
                            var errorMessage = error.stack || error;
                            result = {status: 'failed', error_message: errorMessage};
                        } else {
                            result = {status: stepResult.getStatus()};
                        }
                        formatter.match({uri:'report.feature', step: {line: currentStep.getLine()}});
                        formatter.result(result);
                        break;
                }
                callback();
            },

            handleAnyStep: function handleAnyStep(step) {
                formatter.step({
                    keyword: step.getKeyword(),
                    name   : step.getName(),
                    line   : step.getLine(),
                });
                currentStep = step;
            }
        };
        return self;
    }

    $scope.runFeature = function(feature) {
        console.log("Running tests");

        $http.get('test/steps/'+feature.name+'.steps.js').success(function(steps) {
            console.log("Retrieved steps");
            $http.get('test/features/'+feature.name+'.feature').success(function(featureSource) {
                console.log("Retrieved Feature");

                var supportCode;
                eval('supportCode   = function() {' + steps + '};');

                $output = $('#output');
                var errors          = $('#errors');
                var errorsContainer = $('#errors-container');

                var cucumber        = Cucumber(featureSource, supportCode);
                var $output         = $output;
                $output.empty();
                var listener        = CucumberHTMLListener($output);
                cucumber.attachListener(listener);

                errors.text('');
                errorsContainer.hide();
                try {
                    cucumber.start(function() {});
                } catch(err) {
                    errorsContainer.show();
                    var errMessage = err.message || err;
                    var buffer = (errors.text() == '' ? errMessage : errors.text() + "\n\n" + errMessage);
                    errors.text(buffer);
                    throw err;
                }
            });
        });
    };

    $('#errors-container').hide();
});