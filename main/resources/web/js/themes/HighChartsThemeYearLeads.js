Highcharts.yearLeads = {
    colors: ["#DF5353", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",
        "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
    chart: {
        backgroundColor: {
            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
            stops: [
                [0, 'rgb(96, 96, 96)'],
                [1, 'rgb(16, 16, 16)']
            ]
        },
        type: 'bar',
        height: 310
    },
    tooltip: {
        headerFormat: '<span style="font-size: 10px">{point.key}</span><br/>',
        pointFormat: 'Leads ontvangen: <b>{point.y}</b><br/>'
    },
    plotOptions: {
        bar: {
            colorByPoint: true
        }
    },
    legend: {
        enabled: false
    },
    title: {
        style: {
            color: '#ffffff'
        }
    },
    subtitle: {
        text: '<img src="img/APIlogo.png" width="70">',
        useHTML: true,
        align: 'right',
        y: 7,
        x: 4
    }
};
