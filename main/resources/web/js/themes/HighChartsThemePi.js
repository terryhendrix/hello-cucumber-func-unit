Highcharts.pi = {
    colors: ["#e20f2a", "#54ad3a", "#1382c3"],
    chart: {
        type: 'pie',
        height: 250,
        margin: [25, 0, 0, 0],
        spacingTop: 0,
        spacingBottom: 0,
        spacingLeft: 0,
        spacingRight: 0
    },
    tooltip: {
        headerFormat: '<span style="font-size: 10px"><b>{point.key}</b></span><br/>',
        pointFormat: 'Loten verkocht: <b>{point.y}</b><br/>'
    },
    plotOptions: {
        pie: {
            size: '100%',
            dataLabels: {
                enabled: true
            },
            showInLegend: false
        }
    },
    title: {
        style: {
            color: '#ff00ff'
        }
    }
};