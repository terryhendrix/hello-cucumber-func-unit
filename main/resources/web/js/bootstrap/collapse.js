// This script fixes the collapse of the mobile menu
$(document).on('click', '.navbar-collapse.in', function (e) {
    log.info("COLLAPSE");
    log.info($(e.target).is('a'));
    log.info($(e.target).attr('class'));

    if ($(e.target).is('a') && ( $(e.target).attr('class').indexOf('dropdown-toggle') === -1) && ( $(e.target).attr('class').indexOf('ignore-collapse') === -1 )) {
        $(this).collapse('hide');
    }
});