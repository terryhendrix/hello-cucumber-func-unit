var fundraiserModule = angular.module('fundraiserModule', ['highcharts-ng', 'serviceModule']);

fundraiserModule.controller('FundraiserController', function ($scope, $rootScope, $routeParams, campaignService, selectionService) {
    $scope.fundraiser = $routeParams.fundraiser;
    selectionService.setFundraiser($scope.fundraiser);

    if(typeof $routeParams.campaignId !== 'undefined') {
        $scope.selectedCampaign = $routeParams.campaignId;
        $scope.singleCampaign = true;
    }
    else {
        delete  $scope.selectedCampaign;
        $scope.singleCampaign = false;
    }

    $scope.category = $routeParams.category;
    $rootScope.checkCategory($scope.category);

    campaignService.loadCampaigns($scope.category, $scope.fundraiser);

    $scope.$watch(campaignService.getCampaigns, function(campaigns) {
        $scope.campaigns = campaigns;
    });

    $scope.$on("$routeChangeStart", function(args) {
        selectionService.setFundraiser(undefined)
    });
});

fundraiserModule.factory("lineChartConfigService", function(dateService){
    var options = {
        'all':[
            'year'
        ],
        'year':[
            'month',
            'week'
        ],
        'month':[
            'day'
        ],
        'week':[
            'day'
        ],
        'day':[
            'hour'
        ]
    };

    var haystack = 'all';
    var needle = options['all'][0];
    var week = 0;
    dateService.getWeekNumber().then(function(res){
        week = res.weekNumber;
    });
    var month = new Date().getMonth()+1;
    var year = new Date().getFullYear();
    var day = new Date().getDate();

    var self = {
        getWeek:function(){
            return week;
        },
        setWeek:function(_week) {
            week = _week;
        },
        getMonth:function(){
            return month;
        },
        setMonth:function(_month) {
            month = _month;
        },
        getDay:function(){
            return day;
        },
        setDay:function(_day) {
            day = _day;
        },
        getYear:function(){
            return year;
        },
        setYear:function(_year) {
            year = _year;
        },
        getOptions: function() {
            return options;
        },
        getConfig: function() {
            return [haystack, needle]
        },
        setConfig:function(haystack,needle){
            self.setHaystack(haystack);
            self.setNeedle(needle);
        },
        getNeedle: function() {
            return needle
        },
        getHaystack: function() {
            return haystack;
        },
        setHaystack: function(_haystack) {
            if(typeof options[_haystack] == "undefined")
                log.error("haystack " + _haystack + " not correct");
            else {
                haystack = _haystack;
                needle = options[_haystack][0];
                log.info("Haystack to " + haystack + ", Needle to "+needle);
            }
        },
        setNeedle: function(_needle) {
            if(options[haystack].exists(function(entry) { return entry == _needle })) {
                needle = _needle;
                log.info("Needle to "+needle);
            }
            else
                log.error("The needle "+_needle+" may not be used with haystack "+haystack);
        }
    };
    return self;
});

fundraiserModule.controller("LineChartController", function($scope, $rootScope, $timeout, $location, selectionService, graphService, lineChartConfigService) {

    if($location.path().indexOf("orders") !== -1)
        selectionService.setCategory("orders");
    else if($location.path().indexOf("leads") !== -1)
        selectionService.setCategory("leads");

    switch ($scope.fundraiser) {
        case "NPL":
            if(selectionService.getCategory() == 'orders')
                $scope.style = Highcharts.redOrders;
            else if(selectionService.getCategory() == 'leads')
                $scope.style = Highcharts.redLeads;

            $scope.sidebarClass = "sidebarStyleVL";
            break;
        case "BGL":
            if(selectionService.getCategory() == 'orders')
                $scope.style = Highcharts.greenOrders;
            else if(selectionService.getCategory() == 'leads')
                $scope.style = Highcharts.greenLeads;

            $scope.sidebarClass = "sidebarStyleVL";
            break;
        case "VL":
            if(selectionService.getCategory() == 'orders')
                $scope.style = Highcharts.blueOrders;
            else if(selectionService.getCategory() == 'leads')
                $scope.style = Highcharts.blueLeads;

            $scope.sidebarClass = "sidebarStyleVL";
            break;
        default:
            console.error("Incorrect fundraiser " + $scope.fundraiser);
    }


    $scope.total = 0;
    $scope.title = "";
    $scope.getSidebarClass = function () {
        return $scope.sidebarClass;
    };

    $scope.chart = {
        cfg: {
            options: $scope.style,
            series: [{data: []}],
            title: {
                text: "Campagne #" + $scope.campaign,
                style: {
                    color: '#ffffff'
                }
            },
            xAxis: {categories: []},
            yAxis: {
                minPadding: 0,
                title: ''
            },
            animation: {
                duration: 2000
            }
        }
    };

    $scope.load = function (query) {
        graphService.query($scope.fundraiser, $scope.category, query).then(function (data) {
            $timeout(function(){
                var blocks =  $scope.convertBlocks(data.blocks);
                var names = blocks.map(function(entry){ return entry[0]});
                $scope.chart.cfg.series[0].data = blocks;
                $scope.chart.cfg.xAxis.categories = names;
                $scope.total = $scope.createTotals(data.blocks);
                $scope.title = $scope.createTitle(query);
            }, 10)
        });
    };

    $scope.createTitle = function(query){
        if(query.haystack == "all")
            return "over de jaren heen";
        else if(query.haystack == "year")
            return "in "+query.year;
        else if(query.haystack == "month")
            return "in "+$scope.translateMonth(query.month) + " " + query.year;
        else if(query.haystack == "week")
            return "in week "+query.week+" van "+query.year;
        else if(query.haystack == "day")
            return "op "+query.day+" "+$scope.translateMonth(query.month)+" "+query.year;
        else {
            log.error("Could not create title");
            return "N/A";
        }
    };
    
    $scope.createTotals = function(blocks) {
        var total = 0;
        blocks.map(function(entry){
            total += entry.amount;
        });
        return total;
    };

    $scope.translateMonth = function(number) {
        return $rootScope.monthToNumber[number];
    };

    $scope.translateDay = function(number) {
        return $rootScope.dayToNumber[number];
    };

    $scope.convertBlocks = function(data) {
        return data.map(function(entry) {
            if( typeof entry.hour !== "undefined" )
                return [entry.hour, entry.amount];

            else if(
                typeof entry.month !== "undefined" &&
                typeof entry.year !== "undefined"  &&
                typeof entry.dayOfTheMonth != "undefined"
            )   return [entry.dayOfTheMonth+"-"+$scope.translateMonth(entry.month), entry.amount];

            else if(
                typeof entry.month !== "undefined" &&
                typeof entry.year !== "undefined"
            )   return [$scope.translateMonth(entry.month)+"-"+entry.year, entry.amount];

            else if(
                typeof entry.weekOfTheYear !== "undefined" &&
                typeof entry.dayOfTheWeek !== "undefined"
            )   return [$scope.translateDay(entry.dayOfTheWeek), entry.amount];

            else if(
                typeof entry.weekOfTheYear !== "undefined" &&
                typeof entry.year !== "undefined"
            )   return [entry.weekOfTheYear+"", entry.amount];

            else if(
                typeof entry.year !== "undefined"
            )   return [entry.year+"", entry.amount];

            else return ["N/A", entry.amount];
        });
    };

    $scope.$watchGroup([
        lineChartConfigService.getHaystack,
        lineChartConfigService.getNeedle,
        lineChartConfigService.getYear,
        lineChartConfigService.getMonth,
        lineChartConfigService.getWeek,
        lineChartConfigService.getDay
    ], function() {
        if(lineChartConfigService.getHaystack() == "all") {
            $scope.cancelRefresh();
            $scope.keepRefreshing({
                campaignId: $scope.campaign,
                haystack: lineChartConfigService.getHaystack(),
                needle: lineChartConfigService.getNeedle()
            });
        }
        else if(lineChartConfigService.getHaystack() == "year") {
            $scope.cancelRefresh();
            $scope.keepRefreshing({
                campaignId: $scope.campaign,
                haystack: lineChartConfigService.getHaystack(),
                needle: lineChartConfigService.getNeedle(),
                year: lineChartConfigService.getYear()
            });
        }
        else if(lineChartConfigService.getHaystack() == "month") {
            $scope.cancelRefresh();
            $scope.keepRefreshing({
                campaignId: $scope.campaign,
                haystack: lineChartConfigService.getHaystack(),
                needle: lineChartConfigService.getNeedle(),
                year: lineChartConfigService.getYear(),
                month: lineChartConfigService.getMonth()
            });

        }
        else if(lineChartConfigService.getHaystack() == "week") {
            $scope.cancelRefresh();
            $scope.keepRefreshing({
                campaignId: $scope.campaign,
                haystack: lineChartConfigService.getHaystack(),
                needle: lineChartConfigService.getNeedle(),
                year: lineChartConfigService.getYear(),
                week: lineChartConfigService.getWeek()
            });

        }
        else if(lineChartConfigService.getHaystack() == "day") {
            $scope.cancelRefresh();
            $scope.keepRefreshing({
                campaignId: $scope.campaign,
                haystack: lineChartConfigService.getHaystack(),
                needle: lineChartConfigService.getNeedle(),
                year: lineChartConfigService.getYear(),
                month: lineChartConfigService.getMonth(),
                day: lineChartConfigService.getDay()
            });
        }
        else
            log.error("Could not update")
    });

    $scope.sidebarText = function () {
        var temp = "";
        if($scope.category == "orders")
            temp = "tickets verkocht";
        else if($scope.category == "leads")
            temp = "leads verzameld";
        return $scope.total+" "+temp+" "+$scope.title;
    };


    $scope.keepRefreshing = function(query) {
        $scope.load(query);
        $scope.cancellable = $timeout(function() { $scope.keepRefreshing(query) }, $rootScope.refreshInterval)
    };

    $scope.cancelRefresh = function() {
        $timeout.cancel($scope.cancellable);
    };

    $scope.$on("$routeChangeStart", function(args) {
        log.info("Page change, stopping refresh");
        $scope.cancelRefresh()
    });
});


fundraiserModule.directive('lineChart', function () {
    return {
        restrict: 'E',
        scope: {
            category: "=",
            fundraiser: "=",
            campaign: "="
        },
        templateUrl: '/modules/fundraiser/line-chart.html',
        controller: "LineChartController"
    }
});