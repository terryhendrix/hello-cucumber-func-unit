/**
 * Created by hariskhan1 on 04/05/15.
 */
var summaryWidgetModule = angular.module('summaryWidgetModule', ['serviceModule']);


summaryWidgetModule.controller("SummaryWidgetController", function($scope, $rootScope, dateService, countService, $timeout)
{
    $rootScope.checkCategory($scope.category);

    if ($scope.fundraiser == "BGL") {
        $scope.logoUrl = "img/BGLlogo.png";
        $scope.backgroundColor = "panel-green";
    }
    else if ($scope.fundraiser == "NPL") {
        $scope.logoUrl = "img/NPLlogo.png";
        $scope.backgroundColor = "panel-red";
    }
    else if($scope.fundraiser == "VL") {
        $scope.logoUrl = "img/VLlogo.png";
        $scope.backgroundColor = "panel-blue";
    }
    else {
        $scope.logoUrl = "";
        $scope.backgroundColor = "panel-black";
    }

    $scope.load = function() {
        dateService.getWeekNumber().then(function(date){
            $scope.year = new Date().getFullYear();
            $scope.month = new Date().getMonth() + 1;
            $scope.weekOfTheYear =  date.weekNumber;
            $scope.dayOfTheMonth = new Date().getDate();
            $scope.hour = new Date().getHours();

            countService.get($scope.fundraiser, $scope.category).then(function(result){
                $scope.total = result;
            });


            countService.query($scope.fundraiser, $scope.category, {year: $scope.year}).then(function(result){
                $scope.thisYear = result;
            });

            countService.query($scope.fundraiser, $scope.category, {month: $scope.month, year: $scope.year}).then(function(result){
                $scope.thisMonth = result;
            });

            countService.query($scope.fundraiser, $scope.category, {year: $scope.year, weekOfTheYear: $scope.weekOfTheYear}).then(function(result){
                $scope.thisWeek = result;
            });

            countService.query($scope.fundraiser, $scope.category, {month: $scope.month, year: $scope.year, dayOfTheMonth: $scope.dayOfTheMonth}).then(function(result) {
                $scope.thisDay = result;
            });

            countService.query($scope.fundraiser, $scope.category, {month: $scope.month, year: $scope.year, dayOfTheMonth: $scope.dayOfTheMonth, hour: $scope.hour}).then(function(result){
                $scope.thisHour = result;
            });
        });
    };

    $scope.keepRefreshing = function() {
        $scope.load();
        $scope.cancellable = $timeout($scope.keepRefreshing, $rootScope.refreshInterval)
    };

    $scope.keepRefreshing();

    $scope.cancelRefresh = function() {
        $timeout.cancel($scope.cancellable);
    };

    $scope.$on("$routeChangeStart", function(args) {
        log.info("Page change, stopping refresh");
        $scope.cancelRefresh()
    });
});

summaryWidgetModule.directive("summaryWidget", function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/summary/summary-widget.html',
        scope: {
            fundraiser: "=",
            category: "="
        },
        controller: 'SummaryWidgetController'
    };
});