/**
 * Created by terryhendrix on 19/05/15.
 */

var log = console;

Array.prototype.exists = function(condition)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if(condition(this[i])) return true;
    }
    return false;
};

Array.prototype.replace = function(condition, entry)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if(condition(this[i]))
            this[i] = entry;
    }
    return this;
};

var dashboardApp = angular.module("dashboardApp" , ['ngRoute', 'summaryWidgetModule', 'headingWidgetModule', 'yearChartModule', 'fundraiserModule', 'healthModule']);

dashboardApp.config(function ($routeProvider) {
        $routeProvider
            .when('/:category/loterijen', {
                controller: "FundraiserController",
                templateUrl: 'fundraiser.html'
            })
            .when('/:category/loterijen/:fundraiser', {
                controller: "FundraiserController",
                templateUrl: 'fundraiser.html'
            })
            .when('/:category/loterijen/:fundraiser/:campaignId', {
                controller: 'FundraiserController',
                templateUrl: 'fundraiser.html'
            })
            .when('/history', {
                templateUrl: 'historyFundraiserCharts.html'
            })
            .when('/:category', {
                controller: "DashboardController",
                templateUrl: 'mainView.html'
            })
            .otherwise({
                redirectTo: '/orders'
            });
    }
);

dashboardApp.run(function($rootScope) {
    log.info("Starting dashboard");
    $rootScope.orders = "orders";
    $rootScope.leads = "leads";
    
    $rootScope.dayToNumber = {
        1:  "Ma",
        2:  "Di",
        3:  "Wo",
        4:  "Do",
        5:  "Vr",
        6:  "Za",
        7:  "Zo"
    };
    
    $rootScope.monthToNumber = {
        1: "Jan",
        2: "Feb",
        3: "Maart",
        4: "April",
        5: "Mei",
        6: "Juni",
        7: "Juli",
        8: "Aug",
        9: "Sep",
        10: "Okt",
        11: "Nov",
        12: "Dec"
    };

    $rootScope.availableCategories = [
        $rootScope.orders,
        $rootScope.leads
    ];

    $rootScope.checkCategory = function(category) {
        if($rootScope.availableCategories.indexOf(category) === -1) {
            log.error("Incorrect category: "+category);
            log.error($rootScope.availableCategories);
        }
    };

    $rootScope.NPL = "NPL";
    $rootScope.BGL = "BGL";
    $rootScope.VL = "VL";

    $rootScope.availableFundraisers = [
        $rootScope.NPL,
        $rootScope.BGL,
        $rootScope.VL
    ];

    $rootScope.checkFundraiser = function(fundraiser) {
        if($rootScope.availableFundraisers.indexOf(fundraiser) === -1) {
            log.error("Incorrect category: "+fundraiser);
            log.error($rootScope.availableFundraisers);
            return;
        }
    };

    $rootScope.refreshInterval = 5000;
});

dashboardApp.filter('translateFundraiser', function () {
    return function (input) {
        switch (input) {
            case "NPL":
                return "Nationale Postcode Loterij";
            case "BGL":
                return "Bankgiro Loterij";
            case "VL":
                return "Vrienden Loterij";
            case "all"  :
                return "Alle loterijen";
        }
    };
});

dashboardApp.controller('DashboardController', function($scope, $routeParams,selectionService) {
    log.info("Loading DashboardController");
    log.info($routeParams);
    if(typeof $routeParams.category !== "undefined") {
        $scope.category = $routeParams.category;
        selectionService.setCategory($scope.category);
    }
});

dashboardApp.controller('MenuController', function($scope, selectionService, campaignService, $location, lineChartConfigService) {
    log.info("Loading MenuController");

    $scope.$watch(selectionService.getFundraiser, function(fundraiser){
        $scope.fundraiser = fundraiser;
    });

    $scope.$watchGroup([
        selectionService.getCategory,
        selectionService.getFundraiser
    ], function() {
        $scope.category = selectionService.getCategory();
        $scope.fundraiser = selectionService.getFundraiser();
        log.info("Switching menu to "+$scope.category);
        log.info("Switching fundraiser to "+$scope.fundraiser);

        if(typeof $scope.fundraiser === "undefined")
            $scope.fundraiserUrlPart = "all";
        else
            $scope.fundraiserUrlPart = $scope.fundraiser;

        campaignService.loadCampaigns($scope.category, $scope.fundraiser);
    });

    $scope.$watch(campaignService.getCampaigns, function(result){
        $scope.campaigns = result;
    });

    $scope.setFundraiser = function(fundraiser){
        selectionService.setFundraiser(fundraiser);
        log.info("Setting fundraiser to "+fundraiser);

        if(typeof fundraiser == "undefined")
            var path = "/"+$scope.category+"/loterijen/all";
        else
            var path = "/"+$scope.category+"/loterijen/"+fundraiser;

        console.log(path);
        $location.path(path);
    };

    $scope.pathContains = function(value) {
        return $location.path().indexOf(value) !== -1;
    };

    $scope.isActive = function(haystack, needle) {
        return haystack == lineChartConfigService.getHaystack() && needle == lineChartConfigService.getNeedle();
    };

    $scope.setConfig = function(haystack, needle) {
        lineChartConfigService.setConfig(haystack, needle);
    };

    $scope.changeYear = function(year) {
        if(!isNaN(year) && year.length == 4) {
            log.info("Setting year "+year);
            lineChartConfigService.setYear(parseInt(year));
        }
    };

    $scope.changeMonth = function(month) {
        if(!isNaN(month) && (month >= 1 && month <= 12)) {
            log.info("Setting month "+month);
            lineChartConfigService.setMonth(parseInt(month));
        }
    };

    $scope.changeWeek = function(week) {
        if(!isNaN(week) && (week >= 1 && week <= 53)) {
            log.info("Setting week "+week);
            lineChartConfigService.setWeek(parseInt(week));
        }
    };

    $scope.changeDay = function(day) {
        if(!isNaN(day) && (day >= 1 && day <= 31)) {
            log.info("Setting day "+day);
            lineChartConfigService.setDay(parseInt(day));
        }
    };

    $scope.$watch(lineChartConfigService.getYear, function(year){
        $scope.year = year;
    });

    $scope.$watch(lineChartConfigService.getMonth, function(month){
        $scope.month = month+"";
    });

    $scope.$watch(lineChartConfigService.getWeek, function(week){
        $scope.week = week;
    });

    $scope.$watch(lineChartConfigService.getDay, function(day){
        $scope.day = day;
    });
});


dashboardApp.controller('HeadController', function($scope, $rootScope, selectionService) {
    log.info("Loading HeadController");

    $scope.$watch(selectionService.getCategory, function(category) {
        $scope.category = category;
        $scope.setTitle(category);
    });

    $scope.setTitle = function(category) {
        if(category == $rootScope.orders)
            $scope.title = "API Dashboard | Orders";
        else if(category == $rootScope.leads)
            $scope.title = "API Dashboard | Leads";
        else
            $scope.title = "API Dashboard";

        log.info("Title set to "+$scope.title+" for "+category);
    };

    $scope.getTitle = function() {
        return $scope.title;
    };
});


