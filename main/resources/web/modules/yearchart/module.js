var yearChartModule = angular.module('yearChartModule', ['highcharts-ng','serviceModule']);

yearChartModule.controller('YearChartController', function ($scope, $rootScope, countService, $timeout, $q) {

    $rootScope.checkCategory($scope.category);

    if($scope.category == $rootScope.orders)
    {
        $scope.title = "API lot transacties per jaar";
        $scope.theme = Highcharts.yearOrders;
    }
    else if($scope.category == $rootScope.leads) {
        $scope.theme = Highcharts.yearLeads;
        $scope.title = "API lead transacties per jaar";
    }

    $scope.chart = {
        cfg: {
            options: $scope.theme,
            series: [{
                data: []
            }],
            title: {
                text: $scope.title,
                style: {
                    color: '#ffffff'
                }
            },
            xAxis: {categories: []},
            yAxis: {
                minPadding: 0,
                title: ''
            }
        }
    };


    $scope.load = function() {
        var currentyear = (new Date).getFullYear();

        function loadYear(year) {
            $scope.loadForYear(year).then(function() {
                if(year > 2013)
                loadYear(year - 1)
            });
        }

        loadYear(currentyear)
    };

    $scope.loadForYear = function(year) {
        return $q(function(success, failure) {
            var count = 0;
            countService.query("NPL", $scope.category, { year: year}).then(function(result){
                count += result.amount;

                countService.query("VL", $scope.category, { year: year}).then(function(result){
                    count += result.amount;

                    countService.query("BGL", $scope.category, { year: year}).then(function(result){
                        count += result.amount;
                        if(!$scope.chart.cfg.series[0].data.exists(function(entry) {
                                return entry[0] == year;
                            }))
                            $scope.chart.cfg.series[0].data.push([year+"", count]);
                        else
                            $scope.chart.cfg.series[0].data.replace(function(entry) { return entry[0] == year; }, [year+"", count]);
                        success()
                    }, failure);
                }, failure);
            }, failure);
        });
    };

    $scope.keepRefreshing = function() {
        $scope.load();
        $scope.cancellable = $timeout($scope.keepRefreshing, $rootScope.refreshInterval)
    };

    $scope.keepRefreshing();

    $scope.cancelRefresh = function() {
        $timeout.cancel($scope.cancellable);
    };

    $scope.$on("$routeChangeStart", function(args) {
        log.info("Page change, stopping refresh");
        $scope.cancelRefresh()
    });

    $scope.isLeads = function() {
        return $scope.category == $rootScope.leads;
    };

    $scope.isOrders = function() {
        return $scope.category == $rootScope.orders;
    };
});
yearChartModule.directive('yearChart', function () {
    return {
        restrict: 'E',
        scope: {
            category: "="
        },
        templateUrl: '/modules/yearchart/year-chart.html',
        controller: "YearChartController"
    }
});