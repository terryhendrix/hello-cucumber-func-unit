
var serviceModule = angular.module('serviceModule', []);


serviceModule.factory("httpService", function($q, $http){
    var self = {
        post:function(url, body) {
            return $q(function (success, failure) {
                $http.post(url, body).success(function (result) {
                    console.info("POST "+url+" "+JSON.stringify(body));
                    console.info(result);
                    success(result);
                }).error(function(reason){
                    console.error("POST "+url+" "+JSON.stringify(body));
                    log.error(reason);
                    failure(reason);
                });
            });
        },
        put:function(url,body){
            return $q(function (success, failure) {
                $http.put(url, body).success(function (result) {
                    console.info("PUT "+url+" "+JSON.stringify(body));
                    console.info(result);
                    success(result);
                }).error(function(reason){
                    console.error("PUT "+url+" "+JSON.stringify(body));
                    log.error(reason);
                    failure(reason);
                });
            });
        },
        get:function(url) {
            return $q(function (success, failure) {
                $http.get(url).success(function (result) {
                    console.info("GET "+url);
                    console.info(result);
                    success(result);
                }).error(function(reason){
                    console.error("GET "+url);
                    log.error(reason);
                    failure(reason);
                });
            });
        },
        delete:function(url) {
            return $q(function (success, failure) {
                $http.delete(url).success(function (result) {
                    console.info("DELETE "+url);
                    console.info(result);
                    success(result);
                }).error(function(reason){
                    console.error("DELETE "+url);
                    log.error(reason);
                    failure(reason);
                });
            });
        }
    };
    return self;
});


serviceModule.factory("dateService", function(httpService){
    var self = {
        getWeekNumber:function() {
            return httpService.get("/api/2.0/date/week-number");
        }
    };

    return self;
});

serviceModule.factory("healthService", function(httpService){
    var self = {
        get:function() {
            return httpService.get("/api/2.0/health");
        },
        serviceWindow:function() {
            return httpService.get("/api/2.0/health/service-window");
        }
    };

    return self;
});

serviceModule.factory("countService", function(httpService){
    var self = {
        get:function(fundraiser, category) {
            return httpService.get("/api/2.0/count/" + category + "/" + fundraiser);
        },
        query:function(fundraiser, category, query) {
            return httpService.post("/api/2.0/count/"+category+"/"+fundraiser, query);
        }
    };

    return self;
});

serviceModule.factory("graphService", function(httpService){
    var self = {
        get:function(fundraiser, category) {
            return httpService.get("/api/2.0/graph/" + category + "/" + fundraiser);
        },
        query:function(fundraiser, category, query) {
            return httpService.post("/api/2.0/graph/"+category+"/"+fundraiser, query);
        }
    };

    return self;
});

serviceModule.factory("campaignService", function(httpService) {
    var campaigns = [];

    var self = {
        loadCampaigns: function(category, fundraiser){
            var url = undefined
            if(typeof fundraiser === "undefined")
                url = "/api/2.0/campaigns/"+category
            else
                url = "/api/2.0/campaigns/"+category+"/"+fundraiser;

            httpService.get(url).then(function(result){
                campaigns = result;
            });
        },
        getCampaigns: function() {
            return campaigns;
        }
    };
    return self;
});

serviceModule.factory("selectionService", function(){
    var category = "orders";
    var fundraiser = undefined;

    var self = {
        setCategory:function(cat) {
            category = cat;
        },
        getCategory:function() {
            return category;
        },
        setFundraiser: function(fund){
            fundraiser = fund;
        },
        getFundraiser: function(){
            return fundraiser;
        }
    };
    return self;
});


