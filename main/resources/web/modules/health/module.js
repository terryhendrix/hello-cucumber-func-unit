/**
 * Created by terryhendrix on 10/02/16.
 */

var healthModule = angular.module('healthModule', ['serviceModule']);


healthModule.controller("HealthWidgetController", function($scope, $rootScope, healthService, $timeout)
{
    $scope.load = function() {
        healthService.get().then(function(res){
            $scope.stats = res;
        });

        healthService.serviceWindow().then(function(res){
            $scope.serviceWindow = res;
        });

        $timeout($scope.load, 10000)
    };

    $scope.load();
});


healthModule.directive("healthWidget", function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/health/health-widget.html',
        controller: 'HealthWidgetController'
    };
});