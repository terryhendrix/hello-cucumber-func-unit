/**
 * Created by hariskhan1 on 04/05/15.
 */
var summaryWidgetModule = angular.module('headingWidgetModule', ['serviceModule']);


summaryWidgetModule.controller("HeadingWidgetController", function($scope, $rootScope, countService, $timeout)
{
    $rootScope.checkCategory($scope.category);

    $scope.values = {};

    $scope.load = function() {
        $scope.years = [
            new Date().getFullYear(),
            new Date().getFullYear()-1,
            new Date().getFullYear()-2
        ];

        log.info("Using years:");
        log.info($scope.years);

        $scope.years.forEach(function(year) {
            log.info("Collecting data for year "+year);
            var count = 0;
            countService.query("NPL", $scope.category, { year: year}).then(function(result){
                count += result.amount;

                countService.query("VL", $scope.category, { year: year}).then(function(result){
                    count += result.amount;

                    countService.query("BGL", $scope.category, { year: year}).then(function(result){
                        count += result.amount;
                        $scope.values[year+""] = count;
                    });
                });
            });
        });
    };

    $scope.keepRefreshing = function() {
        $scope.load();
        $scope.cancellable = $timeout($scope.keepRefreshing, $rootScope.refreshInterval)
    };

    $scope.keepRefreshing();

    $scope.cancelRefresh = function() {
        $timeout.cancel($scope.cancellable);
    };

    $scope.$on("$routeChangeStart", function(args) {
        log.info("Page change, stopping refresh");
        $scope.cancelRefresh()
    });

    $scope.isLeads = function() {
        return $scope.category == $rootScope.leads;
    };

    $scope.isOrders = function() {
        return $scope.category == $rootScope.orders;
    };
});

summaryWidgetModule.directive("headingWidget", function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/heading/heading-widget.html',
        scope: {
            category: "="
        },
        controller: 'HeadingWidgetController'
    };
});