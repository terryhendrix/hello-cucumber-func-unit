/**
 * A sample application
 */
var app = angular.module("sample-app", []);

app.controller("AppController", function($scope) {
    $scope.list = [];

    $scope.addRow = function(name, phone, mail) {
        console.log("Adding row");

        $scope.list.push({
            name:name,
            phone:phone,
            mail:mail
        });

        $("#addRowModal").modal("hide");

        //clear out the text fields
        delete $scope.name;
        delete $scope.phone;
        delete $scope.mail;
    };

    $scope.deleteRow = function(row) {
        console.log("Deleting row");

        $scope.list = $scope.list.filter(function(entry) {
            return entry != row;
        })
    };

    $scope.clear = function() {
        console.log("Clearing list");
        $scope.list = [];
    };

    console.log("Loaded app controller");
});

app.controller("TestController", function($scope) {
    function CucumberHTMLListener($root) {
        var formatter = new CucumberHTML.DOMFormatter($root);

        formatter.uri('report.feature');

        var currentStep;

        var self = {
            hear: function hear(event, callback) {
                var eventName = event.getName();
                switch (eventName) {
                    case 'BeforeFeature':
                        var feature = event.getPayloadItem('feature');
                        formatter.feature({
                            keyword     : feature.getKeyword(),
                            name        : feature.getName(),
                            line        : feature.getLine(),
                            description : feature.getDescription()
                        });
                        break;

                    case 'BeforeScenario':
                        var scenario = event.getPayloadItem('scenario');
                        formatter.scenario({
                            keyword     : scenario.getKeyword(),
                            name        : scenario.getName(),
                            line        : scenario.getLine(),
                            description : scenario.getDescription()
                        });
                        break;

                    case 'BeforeStep':
                        var step = event.getPayloadItem('step');
                        self.handleAnyStep(step);
                        break;

                    case 'StepResult':
                        var result;
                        var stepResult = event.getPayloadItem('stepResult');
                        if (stepResult.getStatus() === Cucumber.Status.FAILED) {
                            var error = stepResult.getFailureException();
                            var errorMessage = error.stack || error;
                            result = {status: 'failed', error_message: errorMessage};
                        } else {
                            result = {status: stepResult.getStatus()};
                        }
                        formatter.match({uri:'report.feature', step: {line: currentStep.getLine()}});
                        formatter.result(result);
                        break;
                }
                callback();
            },

            handleAnyStep: function handleAnyStep(step) {
                formatter.step({
                    keyword: step.getKeyword(),
                    name   : step.getName(),
                    line   : step.getLine(),
                });
                currentStep = step;
            }
        };
        return self;
    }

    $scope.runFeature = function() {
        console.log("Running tests");
        jQuery.get('test/steps/increment.steps.js', function(data) {
            alert(data);
        });

        // Untouched so far...
        var supportCode;
        $output = $('#output');
        var output          = $output;
        var errors          = $('#errors');
        var errorsContainer = $('#errors-container');
        var featureSource   = $('#feature').val();
        eval('supportCode   = function() {' + $('#step-definitions').val() + '};');
        var cucumber        = Cucumber(featureSource, supportCode);
        var $output         = $output;
        $output.empty();
        var listener        = CucumberHTMLListener($output);
        cucumber.attachListener(listener);

        errors.text('');
        errorsContainer.hide();
        try {
            cucumber.start(function() {});
        } catch(err) {
            errorsContainer.show();
            var errMessage = err.message || err;
            var buffer = (errors.text() == '' ? errMessage : errors.text() + "\n\n" + errMessage);
            errors.text(buffer);
            throw err;
        }
    };

    $('#errors-container').hide();
});