## Hello Cucumber / FuncUnit

A sample application with same sample tests. Once the project is complete, it will give a framework for cucumber + func unit to work. So far we can test different features, the project contains the math and contacts feature.

The math feature is a simple cucumber only test. 

The contacts feature is slightly more complicated and includes an angularJS frontend, which can be used to manage contact information.

### Running example 
To start the project, use `sbt run` from the command line. Then go to [localhost:8080](http://localhost:8080) for the contacts information app, and go to [localhost:8080/test.html](http://localhost:8080/test.html) for the tester.

### How to use

1. Fork the project and modify it the way you like. 
2. Add your own features. 
3. Goto test.html and run your feature.

### References

* [Cucumber and FuncUnit explained](http://retroaktive.me/blog/funcunit-and-cucumber-a-perfect-combo-for-frontend-testing/)
* [Tutorial cucumber](https://semaphoreci.com/community/tutorials/introduction-to-writing-acceptance-tests-with-cucumber)
* [Bootstrap](http://getbootstrap.com/)
* [FuncUnit](https://funcunit.com/)
